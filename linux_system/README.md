# fix language of language of "time" (data utility) (like names of months, days, ...)

data utility uses variable LC_TIME to know what language of days to use
- for temporary change:
```bash
LC_TIME=en_US.utf8
```

- for permanent change edit the value in /etc/default/locale

