# description

KDE plasma theme broken (all themes not just one particular).

## symptoms:

- theme is light although
- some Icons are not displayed anymore
- strange artifacts <a rel="strange-artifacts"></a>
- <a href="screenshots/strangeArtifacts.png">strange artifacts</a> (I don't know how to describe it better. There is an example screenshot below.)

## screenshots:


![](./screenshots/Screenshot_20230813_145752.png)


![](./screenshots/application_dashboard.png)

![strange-artifacts](./screenshots/strangeArtifacts.png "strange-artifacts")

![](./screenshots/Screenshot_20230813_135831.png)


# fix

check the variable `QT_QPA_PLATFORMTHEME` by executing the following.

```bash
echo $QT_QPA_PLATFORMTHEME
```

In my case it was set to qt5ct witch caused the issue.
If it is set on your computer as well try to find the file where it set and delete the corresponding line.
In my case it was set in `/etc/environment`. If in your case it is set somewhere else look for it in the files mentioned by <a href="https://help.ubuntu.com/community/EnvironmentVariables">this article</a>.
After that you have to log out and back in for the changes to take affect. (If it is not working try to restart.)


# references regarding this issue

- https://www.reddit.com/r/archlinux/comments/nyrktm/kde_plasma_settings_is_light_despite_dark_theme/?rdt=42252
- https://www.reddit.com/user/altermeetax/
- https://help.ubuntu.com/community/EnvironmentVariables
