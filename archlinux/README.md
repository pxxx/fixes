
---------------------
---------------------

# fix dependency conflicts

> Alternatively to the following you can also just deinstall a conflicting package and install it after the update. If the package does not happen to be a dependency for a lot of other stuff this can be strait forward.

## update all packages that do not cause a conflict

```bash
pacman -Syu --ignore ttf-dejavu
```

## force installation of package even when it has dependency conflicts

```bash
pacman -S --force ttf-dejavu
```

---------------------
---------------------

# pacman key problems

https://wiki.archlinux.de/title/Pacman-key

## refresh keys

```bash
pacman-key --refresh-keys
```

---------------------
---------------------

# remove old cache files of pacman
```bash
sudo paccache -r
```
